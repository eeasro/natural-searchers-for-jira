package sk.eea.jira.searchers.natural;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.customfields.SingleValueCustomFieldValueProvider;
import com.atlassian.jira.issue.customfields.searchers.SimpleCustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.information.CustomFieldSearcherInformation;
import com.atlassian.jira.issue.customfields.searchers.renderer.CustomFieldRenderer;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.searchers.transformer.ExactTextCustomFieldSearchInputTransformer;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.index.indexers.impl.ExactTextCustomFieldIndexer;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.query.ActualValueCustomFieldClauseQueryFactory;
import com.atlassian.jira.jql.util.SimpleIndexValueConverter;
import com.atlassian.jira.jql.validator.ExactTextCustomFieldValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.FieldVisibilityManager;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/**
 * ExactTextSearcher reuses TextSearcher with exact searching settings.
 */
public class ExactTextSearcher extends TextSearcher implements CustomFieldStattable {

    public ExactTextSearcher(JqlOperandResolver jqlOperandResolver, CustomFieldInputHelper customFieldInputHelper,
                             FieldVisibilityManager fieldVisibilityManager, JiraAuthenticationContext jiraAuthenticationContext) {
        super(jqlOperandResolver, customFieldInputHelper, fieldVisibilityManager, jiraAuthenticationContext);
    }

    @Override
    public LuceneFieldSorter<?> getSorter(CustomField customField) {
        return new TextFieldNaturalSorter(customField.getId(), true);
    }

    @Override
    public StatisticsMapper getStatisticsMapper(CustomField customField) {
        return new ExactTextStatisticsMapper(customField, jiraAuthenticationContext, customFieldInputHelper);
    }

    @Override
    public void init(CustomField field) {
        final ClauseNames names = field.getClauseNames();
        final FieldIndexer indexer = new ExactTextCustomFieldIndexer(fieldVisibilityManager, field);
        searcherInformation = new CustomFieldSearcherInformation(field.getId(), field.getNameKey(),
                Collections.singletonList(indexer), new AtomicReference<CustomField>(field));
        searchRenderer = new CustomFieldRenderer(names, getDescriptor(), field, new SingleValueCustomFieldValueProvider(),
                fieldVisibilityManager);
        searchInputTransformer = new ExactTextCustomFieldSearchInputTransformer(field, names, searcherInformation.getId(),
                customFieldInputHelper);
        customFieldSearcherClauseHandler = new SimpleCustomFieldSearcherClauseHandler(new ExactTextCustomFieldValidator(),
                new ActualValueCustomFieldClauseQueryFactory(field.getId(), jqlOperandResolver, new SimpleIndexValueConverter(false), false),
                OperatorClasses.EQUALITY_OPERATORS_WITH_EMPTY, JiraDataTypes.TEXT);

    }

}
