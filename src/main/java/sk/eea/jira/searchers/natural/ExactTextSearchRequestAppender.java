package sk.eea.jira.searchers.natural;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestAppender;
import com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.query.operator.Operator;

import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndClause;
import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndNotClauses;

/**
 * Adds support for search operators to our text searchers.
 */
class ExactTextSearchRequestAppender implements SearchRequestAddendumBuilder.AddendumCallback<String>, SearchRequestAppender<String> {
    final String clauseName;
    private Operator operator;

    public ExactTextSearchRequestAppender(String clauseName, Operator operator) {
        this.clauseName = Assertions.notNull("clauseName", clauseName);
        this.operator = operator;
    }

    @Override
    public void appendNonNullItem(String value, JqlClauseBuilder clauseBuilder) {
        clauseBuilder.addStringCondition(clauseName, operator, value);
    }

    @Override
    public void appendNullItem(JqlClauseBuilder clauseBuilder) {
        clauseBuilder.addEmptyCondition(clauseName);
    }

    @Override
    public SearchRequest appendInclusiveSingleValueClause(String value, SearchRequest searchRequest) {
        return appendAndClause(value, searchRequest, this);
    }

    @Override
    public SearchRequest appendExclusiveMultiValueClause(Iterable<? extends String> values, SearchRequest searchRequest) {
        return appendAndNotClauses(values, searchRequest, this);
    }
}
