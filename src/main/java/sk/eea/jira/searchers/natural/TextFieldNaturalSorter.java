package sk.eea.jira.searchers.natural;

/**
 * Provides sorter with (in)sensitive natural comparator.
 *
 * @author Jozef Kotlar
 */

import com.atlassian.jira.issue.statistics.TextFieldSorter;
import com.atlassian.jira.util.NaturalOrderStringComparator;

import java.util.Comparator;

public class TextFieldNaturalSorter extends com.atlassian.jira.issue.statistics.TextFieldSorter {
    private final Comparator<String> comparator;

	public TextFieldNaturalSorter(final String documentConstant, boolean sensitive) {
		super(documentConstant);
        this.comparator = sensitive ? NaturalOrderStringComparator.CASE_SENSITIVE_ORDER : NaturalOrderStringComparator.CASE_INSENSITIVE_ORDER;
	}

	@Override
	public Comparator<String> getComparator() {
		return comparator;
	}
}
