package sk.eea.jira.searchers.natural;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.customfields.SingleValueCustomFieldValueProvider;
import com.atlassian.jira.issue.customfields.SortableCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.AbstractInitializationCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.CustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.SimpleAllTextCustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.information.CustomFieldSearcherInformation;
import com.atlassian.jira.issue.customfields.searchers.renderer.CustomFieldRenderer;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.searchers.transformer.FreeTextCustomFieldSearchInputTransformer;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.index.indexers.impl.SortableTextCustomFieldIndexer;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.search.searchers.information.SearcherInformation;
import com.atlassian.jira.issue.search.searchers.renderer.SearchRenderer;
import com.atlassian.jira.issue.search.searchers.transformer.SearchInputTransformer;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.query.FreeTextClauseQueryFactory;
import com.atlassian.jira.jql.validator.FreeTextFieldValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.FieldVisibilityManager;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Modifies standard searcher to use the natural order for strings.
 * 
 * @author Jozef Kotlar
 * @author Peter Csiba
 */
public class TextSearcher extends AbstractInitializationCustomFieldSearcher implements SortableCustomFieldSearcher {

    protected final FieldVisibilityManager fieldVisibilityManager;
    protected final JqlOperandResolver jqlOperandResolver;
    protected final CustomFieldInputHelper customFieldInputHelper;
    protected final JiraAuthenticationContext jiraAuthenticationContext;

    protected volatile CustomFieldSearcherInformation searcherInformation;
    protected volatile SearchInputTransformer searchInputTransformer;
    protected volatile SearchRenderer searchRenderer;
    protected volatile CustomFieldSearcherClauseHandler customFieldSearcherClauseHandler;

    public TextSearcher(JqlOperandResolver jqlOperandResolver, CustomFieldInputHelper customFieldInputHelper,
                             FieldVisibilityManager fieldVisibilityManager, JiraAuthenticationContext jiraAuthenticationContext) {
        this.fieldVisibilityManager = fieldVisibilityManager;
        this.jqlOperandResolver = jqlOperandResolver;
        this.customFieldInputHelper = customFieldInputHelper;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public final SearcherInformation<CustomField> getSearchInformation() {
        if (searcherInformation == null) {
            throw new IllegalStateException("Attempt to retrieve SearcherInformation off uninitialised custom field searcher.");
        }
        return searcherInformation;
    }

    @Override
    public final SearchInputTransformer getSearchInputTransformer() {
        if (searchInputTransformer == null) {
            throw new IllegalStateException("Attempt to retrieve searchInputTransformer off uninitialised custom field searcher.");
        }
        return searchInputTransformer;
    }

    @Override
    public final SearchRenderer getSearchRenderer() {
        if (searchRenderer == null) {
            throw new IllegalStateException("Attempt to retrieve searchRenderer off uninitialised custom field searcher.");
        }
        return searchRenderer;
    }

    @Override
    public final CustomFieldSearcherClauseHandler getCustomFieldSearcherClauseHandler() {
        if (customFieldSearcherClauseHandler == null) {
            throw new IllegalStateException("Attempt to retrieve customFieldSearcherClauseHandler off uninitialised custom field searcher.");
        }
        return customFieldSearcherClauseHandler;
    }


    @Override
    public LuceneFieldSorter<?> getSorter(CustomField customField) {
        return new TextFieldNaturalSorter(customField.getId(), false);
    }

    @Override
    public void init(CustomField field) {

        searcherInformation = new CustomFieldSearcherInformation(field.getId(), field.getNameKey(),
                Collections.<FieldIndexer> singletonList(new SortableTextCustomFieldIndexer(fieldVisibilityManager, field,
                        DocumentConstants.LUCENE_SORTFIELD_PREFIX)), new AtomicReference<CustomField>(field));
        searchRenderer = new CustomFieldRenderer(field.getClauseNames(), getDescriptor(), field, new SingleValueCustomFieldValueProvider(),
                fieldVisibilityManager);
        searchInputTransformer = new FreeTextCustomFieldSearchInputTransformer(field, field.getClauseNames(),
                searcherInformation.getId(), customFieldInputHelper);
        customFieldSearcherClauseHandler = new SimpleAllTextCustomFieldSearcherClauseHandler(new FreeTextFieldValidator(field.getId(),
                jqlOperandResolver), new FreeTextClauseQueryFactory(jqlOperandResolver, field.getId()), OperatorClasses.TEXT_OPERATORS,
                JiraDataTypes.TEXT);
    }
}
