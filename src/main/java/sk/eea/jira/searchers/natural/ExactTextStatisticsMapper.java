package sk.eea.jira.searchers.natural;

import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestAppender;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.NaturalOrderStringComparator;
import com.atlassian.query.operator.Operator;

import java.util.Comparator;

public class ExactTextStatisticsMapper implements StatisticsMapper<String>, SearchRequestAppender.Factory<String> {

    private final CustomField customField;
    private final CustomFieldInputHelper customFieldInputHelper;
    private final JiraAuthenticationContext	authenticationContext;

    public ExactTextStatisticsMapper(CustomField customField, final JiraAuthenticationContext authenticationContext,
                                    final CustomFieldInputHelper customFieldInputHelper) {
        this.customField = customField;
        this.customFieldInputHelper = customFieldInputHelper;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final String that = ((ExactTextStatisticsMapper) o).getDocumentConstant();

        return ((getDocumentConstant() != null) ? getDocumentConstant().equals(that) : that == null);
    }

    @Override
    public Comparator<String> getComparator() {
        return NaturalOrderStringComparator.CASE_INSENSITIVE_ORDER;
    }

    @Override
    public String getDocumentConstant() {
        return customField.getId();
    }

    @Deprecated
    @Override
    public SearchRequest getSearchUrlSuffix(String value, SearchRequest searchRequest) {
        return getSearchRequestAppender().appendInclusiveSingleValueClause(value, searchRequest);
    }

    @Override
    public String getValueFromLuceneField(String documentValue) {
        return documentValue;
    }

    @Override
    public int hashCode() {
        return ((getDocumentConstant() != null) ? getDocumentConstant().hashCode() : 0);
    }

    @Override
    public boolean isFieldAlwaysPartOfAnIssue() {
        return false;
    }

    @Override
    public boolean isValidValue(String value) {
        return value != null;
    }

    /**
     * Required for supporting search.
     */

    @Override
	public SearchRequestAppender<String> getSearchRequestAppender() {
		return new ExactTextSearchRequestAppender(customFieldInputHelper.getUniqueClauseName(authenticationContext.getUser().getDirectoryUser(),
				customField.getClauseNames().getPrimaryName(), customField.getName()), Operator.EQUALS);
	}

}