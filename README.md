# Descripttion

JIRA plugin adds text field searcher with natural order. E.g. z9.bat is before z10.bat as would a human expects.

There are actually two searchers:
- _Free text searcher (natural)_ should behave as built-in searcher, just provides natural sorting. 
- _Exact text searcher (natural)_ provides also possibility to use text field in statistical gadgets.

# Configuration

Go to _Administration_->_Issues_->_Custom fields_->_Edit_ action and set one of the above searchers. Don't forget to reindex!

# Note 

There is more up-to-date fork project - [Stattable searchers](https://marketplace.atlassian.com/plugins/eu.kprod.jira.stattable-searchers) 
with more functionality.